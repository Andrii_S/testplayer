//
//  ViewController.m
//  TestPlayer
//
//  Created by Andrij Sosna on 16.12.13.
//  Copyright (c) 2013 Andrij Sosna. All rights reserved.
//

#import "ViewController.h"

#import "BaseVideoPlayer.h"

@interface ViewController () <BaseVideoPlayerDelegate>
{
	BaseVideoPlayer* _player;
	BaseVideoPlayerView *_playerView;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	self.view.backgroundColor = [UIColor brownColor];
	
	_player  = [[BaseVideoPlayer alloc] init];
	
	NSURL *videoURL = [[NSBundle mainBundle] URLForResource:@"Amit" withExtension:@"mov"];
	
	_playerView = _player.playerView;
	_playerView.frame = self.view.bounds;
	[self.view addSubview:_playerView];
	
	_player.asset = [AVURLAsset assetWithURL:videoURL];
	[_player setPlayerView:_playerView];

	_player.muted = YES;
	_player.delegate = self;
	[_player seekToTime:CMTimeMakeWithSeconds(10, 60)];
	[_player play];
	
//	[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(resizePlayerView) userInfo:nil repeats:YES];
}

//- (void)resizePlayerView
//{
//	NSUInteger width = arc4random() % 320;
//	NSUInteger height = arc4random() % 568;
//	[UIView animateWithDuration:1
//					 animations:^{
//						 _playerView.frame = CGRectMake(0, 0, width, height);
//					 }];
////	_playerView.frame = CGRectMake(0, 0, width, height);
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)videoPlayerDidReachEnd:(BaseVideoPlayer *)player
{
	NSLog(@"Item did reach end");
}

@end
