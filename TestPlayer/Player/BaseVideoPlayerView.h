//
//  BaseVideoPlayerView.h
//  Takes
//
//  Created by Andrij Sosna on 16.12.13.
//
//

@import UIKit;

@class AVPlayer;

@interface BaseVideoPlayerView : UIView

@property (nonatomic, retain) AVPlayer* player;

- (void)setPlayer:(AVPlayer*)player;
- (void)setVideoFillMode:(NSString *)fillMode;

@end
