//
//  BasePlayer.h
//  Takes
//
//  Created by Andrij Sosna on 13.12.13.
//
//

@import AVFoundation;

#import "BaseVideoPlayerView.h"

@class BaseVideoPlayer;

@protocol BaseVideoPlayerDelegate <NSObject>

- (void)videoPlayerDidReachEnd:(BaseVideoPlayer *)player;

//- (void)videoPlayerDifFail:(BaseVideoPlayer *)player;

// maybe we'll need something like "didStart" and/or "ready to play"

@end

@interface BaseVideoPlayer : NSObject
{
	@protected

	BOOL _seekToZeroBeforePlay;

	CMTime _timeToSeekToWhenReady;
	BOOL _shouldSeekToTimeWhenReady;
	BOOL _shouldStartPlayingWhenReady;
	
	AVPlayer *_internalPlayer;
	AVPlayerItem *_playerItem;
}

@property (nonatomic) AVAsset *asset;
@property (nonatomic) BaseVideoPlayerView *playerView;

@property id<BaseVideoPlayerDelegate> delegate;

@property (readonly) BOOL readyToPlay;

/**
 @property muted
 @warning YES by default
 */
@property (getter = isMuted) BOOL muted;

//- (instancetype)initWithFrame:(CGRect)frame videoURL:(NSURL *)videoURL;


- (void)play;
- (void)pause;

- (void)seekToTime:(CMTime)time; // maybe with completionHandler

// Maybe we'll need seekToStart, seekToCaptureTime and playFromTime and so on

@end
