//
//  BasePlayer.m
//  Takes
//
//  Created by Andrij Sosna on 13.12.13.
//
//

#import "BaseVideoPlayer.h"

static void *BaseVideoPlayerRateObservationContext = &BaseVideoPlayerRateObservationContext;
static void *BaseVideoPlayerStatusObservationContext = &BaseVideoPlayerStatusObservationContext;
static void *BaseVideoPlayerCurrentItemObservationContext = &BaseVideoPlayerCurrentItemObservationContext;

/* Asset keys */
NSString * const kTracksKey         = @"tracks";
NSString * const kPlayableKey		= @"playable";

/* PlayerItem keys */
NSString * const kStatusKey         = @"status";

/* AVPlayer keys */
NSString * const kRateKey			= @"rate";
NSString * const kCurrentItemKey	= @"currentItem";

@implementation BaseVideoPlayer

- (void)setAsset:(AVAsset *)asset
{
	if (_asset != asset)
	{
		_asset = asset;
		
		[self willChangeValueForKey:@"readyToPlay"];
		_readyToPlay = NO;
		[self didChangeValueForKey:@"readyToPlay"];
		
        /*
         Create an asset for inspection of a resource referenced by a given URL.
         Load the values for the asset keys "tracks", "playable".
         */
        NSArray *requestedKeys = [NSArray arrayWithObjects:kTracksKey, kPlayableKey, nil];
		
        /* Tells the asset to load the values of any of the specified keys that are not already loaded. */
        [_asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{
             dispatch_async( dispatch_get_main_queue(),
                            ^{
                                /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
                                [self prepareToPlayAsset:_asset withKeys:requestedKeys];
                            });
         }];
	}
}

- (void)play
{
	NSLog(@"Play");
	if (!_readyToPlay)
	{
		_shouldStartPlayingWhenReady = YES;
		return;
	}
	
	/* If we are at the end of the movie, we must seek to the beginning first
	 before starting playback. */
	if (_seekToZeroBeforePlay)
	{
		_seekToZeroBeforePlay = NO;
		[_internalPlayer seekToTime:kCMTimeZero];
	}
	
	_internalPlayer.muted = _muted;
	[_internalPlayer play];
}

- (void)pause
{
	[_internalPlayer pause];
}

- (void)seekToTime:(CMTime)time
{
	if (!_readyToPlay)
	{
		_shouldSeekToTimeWhenReady = YES;
		_timeToSeekToWhenReady = time;
		return;
	}
	
	[_internalPlayer seekToTime:time];
}

/*
 Invoked at the completion of the loading of the values for all keys on the asset that we require.
 Checks whether loading was successfull and whether the asset is playable.
 If so, sets up an AVPlayerItem and an AVPlayer to play the asset.
 */
- (void)prepareToPlayAsset:(AVAsset *)asset withKeys:(NSArray *)requestedKeys
{
    /* Make sure that the value of each key has loaded successfully. */
	for (NSString *key in requestedKeys)
	{
		NSError *error = nil;
		AVKeyValueStatus keyStatus = [asset statusOfValueForKey:key error:&error];
		if (keyStatus == AVKeyValueStatusFailed)
		{
			NSLog(@"ERROR: %@", error);
//			[self assetFailedToPrepareForPlayback:error];
//			[self stopLoadingAnimationAndHandleError:error];
			return;
		}
		/* If you are also implementing -[AVAsset cancelLoading], add your code here to bail out properly in the case of cancellation. */
	}
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable)
    {
        /* Generate an error describing the failure. */
		NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
		NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
		NSDictionary *errorDict = @{NSLocalizedDescriptionKey : localizedDescription,
									NSLocalizedFailureReasonErrorKey :localizedFailureReason};
		NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];
        
        /* Display the error to the user. */
		NSLog(@"ERROR: %@", assetCannotBePlayedError);
//        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];
        
        return;
    }
	
	if (!asset.composable)
	{
		// Asset cannot be used to create a composition (e.g. it may have protected content).
		NSLog(@"ERROR: Asset can't be used to create a composition. Possibly it contain protected content");
//		[self stopLoadingAnimationAndHandleError:nil];
//		[[self protectedVideoLabel] setHidden:NO];
		return;
	}
	
	/* At this point we're ready to set up for playback of the asset. */
	[self willChangeValueForKey:@"readyToPlay"];
	_readyToPlay = YES;
	[self didChangeValueForKey:@"readyToPlay"];

    /* Stop observing our prior AVPlayerItem, if we have one. */
    if (_playerItem)
    {
        /* Remove existing player item key value observers and notifications. */
        
        [_playerItem removeObserver:self forKeyPath:kStatusKey];
		
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:_playerItem];
    }
	
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    _playerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [_playerItem addObserver:self
					  forKeyPath:kStatusKey
						 options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
						 context:BaseVideoPlayerStatusObservationContext];
	
    /* When the player item has played to its end time we'll toggle
     the movie controller Pause button to be the Play button */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:_playerItem];
	
    _seekToZeroBeforePlay = NO;
	
    /* Create new player, if we don't already have one. */
    if (!_internalPlayer)
    {
        /* Get a new AVPlayer initialized to play the specified player item. */
        _internalPlayer =[AVPlayer playerWithPlayerItem:_playerItem];
		
        /* Observe the AVPlayer "currentItem" property to find out when any
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
         occur.*/
        [_internalPlayer addObserver:self
                      forKeyPath:kCurrentItemKey
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:BaseVideoPlayerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [_internalPlayer addObserver:self
                      forKeyPath:kRateKey
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:BaseVideoPlayerRateObservationContext];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (_internalPlayer.currentItem != _playerItem)
    {
        /* Replace the player item with a new player item. The item replacement occurs
         asynchronously; observe the currentItem property to find out when the
         replacement will/did occur*/
        [_internalPlayer replaceCurrentItemWithPlayerItem:_playerItem];
    }
}

- (BaseVideoPlayerView *)playerView
{
	if (!_playerView) {
		_playerView = [[BaseVideoPlayerView alloc] init];
	}
	return _playerView;
}

#pragma mark Player Item

- (BOOL)isPlaying
{
	return [_internalPlayer rate] != 0.f;
}

/* Called when the player item has played to its end time. */
- (void)playerItemDidReachEnd:(NSNotification *)notification
{
	/* After the movie has played to its end time, seek back to time zero
	 to play it again. */
	_seekToZeroBeforePlay = YES;
	
	[_delegate videoPlayerDidReachEnd:self];
}

- (CMTime)playerItemDuration
{
	AVPlayerItem *playerItem = [_internalPlayer currentItem];
	if (playerItem.status == AVPlayerItemStatusReadyToPlay)
	{
        /*
         NOTE:
         Because of the dynamic nature of HTTP Live Streaming Media, the best practice
         for obtaining the duration of an AVPlayerItem object has changed in iOS 4.3.
         Prior to iOS 4.3, you would obtain the duration of a player item by fetching
         the value of the duration property of its associated AVAsset object. However,
         note that for HTTP Live Streaming Media the duration of a player item during
         any particular playback session may differ from the duration of its asset. For
         this reason a new key-value observable duration property has been defined on
         AVPlayerItem.
         
         See the AV Foundation Release Notes for iOS 4.3 for more information.
         */
		
		return([playerItem duration]);
	}
	
	return(kCMTimeInvalid);
}

/* Cancels the previously registered time observer. */
//-(void)removePlayerTimeObserver
//{
//	if (_timeObserver) {
//		[self.player removeTimeObserver:_timeObserver];
//	}
//}

//- (void)setUpPlaybackOfAsset:(AVAsset *)asset withKeys:(NSArray *)keys
//{
//	// This method is called when AVAsset has completed loading the specified array of keys.
//	// playback of the asset is set up here.
//	
//	// Check whether the values of each of the keys we need has been successfully loaded.
//	for (NSString *key in keys) {
//		NSError *error = nil;
//		
//		if ([asset statusOfValueForKey:key error:&error] == AVKeyValueStatusFailed) {
//			[self stopLoadingAnimationAndHandleError:error];
//			return;
//		}
//	}
//	
//	if (![asset isPlayable]) {
//		// Asset cannot be played. Display the "Unplayable Asset" label.
//		[self stopLoadingAnimationAndHandleError:nil];
//		[[self unplayableLabel] setHidden:NO];
//		return;
//	}
//	
//	if (![asset isComposable]) {
//		// Asset cannot be used to create a composition (e.g. it may have protected content).
//		[self stopLoadingAnimationAndHandleError:nil];
//		[[self protectedVideoLabel] setHidden:NO];
//		return;
//	}
//	
//	// Set up an AVPlayerLayer
//	if ([[asset tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
//		// Create an AVPlayerLayer and add it to the player view if there is video, but hide it until it's ready for display
//		AVPlayerLayer *newPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:[self player]];
//		[newPlayerLayer setFrame:[[[self playerView] layer] bounds]];
//		[newPlayerLayer setHidden:YES];
//		[[[self playerView] layer] addSublayer:newPlayerLayer];
//		[self setPlayerLayer:newPlayerLayer];
//		[self addObserver:self forKeyPath:@"playerLayer.readyForDisplay" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVSEPlayerLayerReadyForDisplay];
//	}
//	else {
//		// This asset has no video tracks. Show the "No Video" label.
//		[self stopLoadingAnimationAndHandleError:nil];
//		[[self noVideoLabel] setHidden:NO];
//	}
//	
//	// Create a new AVPlayerItem and make it the player's current item.
//	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
//	[[self player] replaceCurrentItemWithPlayerItem:playerItem];
//}

#pragma mark - KVO methods
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == BaseVideoPlayerStatusObservationContext)
	{
//		[self syncPlayPauseButtons];
		
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
			// Indicates that the status of the player is not yet known because it has not tried to load new media resources for playback
            case AVPlayerStatusUnknown:
            {
//                [self removePlayerTimeObserver];
//                [self disablePlayerButtons];
            }
				break;
                
            case AVPlayerStatusReadyToPlay:
            {
				// Now duration can be fetched from the item
//                [self enablePlayerButtons];
				
				NSLog(@"AVPlayerStatusReadyToPlay");
				
				[self willChangeValueForKey:@"readyToPlay"];
				_readyToPlay = YES;
				[self didChangeValueForKey:@"readyToPlay"];
				
				if (_shouldSeekToTimeWhenReady && CMTimeCompare(_internalPlayer.currentTime, _timeToSeekToWhenReady))
				{
					[self seekToTime:_timeToSeekToWhenReady];
					_shouldSeekToTimeWhenReady = NO;
				}
				
				if (_shouldStartPlayingWhenReady)
				{
					[self play];
					_shouldStartPlayingWhenReady = NO;
				}
				
            }
				break;
                
            case AVPlayerStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
				NSLog(@"ERROR: %@", playerItem.error);
//                [self assetFailedToPrepareForPlayback:playerItem.error];
            }
				break;
        }
	}
	
	else if (context == BaseVideoPlayerRateObservationContext)
	{
//        [self syncPlayPauseButtons];
	}

	 //Current item observer. Called when the AVPlayer replaceCurrentItemWithPlayerItem: will/did occur
	else if (context == BaseVideoPlayerCurrentItemObservationContext)
	{
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
//            [self disablePlayerButtons];
			[self willChangeValueForKey:@"readyToPlay"];
			_readyToPlay = NO;
			[self didChangeValueForKey:@"readyToPlay"];
        }
        else /* Replacement of player currentItem has occurred */
        {
            /* Set the AVPlayer for which the player layer displays visual output. */
            [self.playerView setPlayer:_internalPlayer];
            
            // Specifies that the player should preserve the video’s aspect ratio fit the video within the layer’s bounds.
            [self.playerView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            
//            [self syncPlayPauseButtons];
        }
	}
	else
	{
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

@end
